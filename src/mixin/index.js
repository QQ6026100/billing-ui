import Vue from "vue";
import { mapGetters } from "vuex";

const globalMixin = {
  // eslint-disable-next-line
  install(Vue) {
    Vue.mixin({
      components: {},
      methods: {},
      computed: {
        ...mapGetters(["screenHeight", "provinceCode"])
      }
    });
  }
};

Vue.use(globalMixin);

export default globalMixin;
