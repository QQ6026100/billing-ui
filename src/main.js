import Vue from "vue";

import Cookies from "js-cookie";

import "normalize.css/normalize.css"; // a modern alternative to CSS resets

import Element from "element-ui";
import "./assets/styles/element-variables.scss";
import "@/mixin";

import "@/assets/styles/index.scss"; // global css
import "@/assets/styles/ruoyi.scss"; // ruoyi css
import App from "./App";
import store from "./store";
import router from "./router";
import permission from "./directive/permission";

import "./assets/icons"; // icon
import "./permission"; // permission control
import { getDicts } from "@/api/system/dict/data";
import { getConfigKey } from "@/api/system/config";
import {
  parseTime,
  resetForm,
  addDateRange,
  selectDictLabel,
  download,
  handleTree
} from "@/utils/ruoyi";
import Pagination from "@/components/Pagination";
import moment from "moment";
import ElDialog from "@/components/dialog";

Vue.use(require("vue-moment"));
Vue.prototype.moment = moment;
// 全局方法挂载
Vue.prototype.getDicts = getDicts;
Vue.prototype.getConfigKey = getConfigKey;
Vue.prototype.parseTime = parseTime;
Vue.prototype.resetForm = resetForm;
Vue.prototype.addDateRange = addDateRange;
Vue.prototype.selectDictLabel = selectDictLabel;
Vue.prototype.download = download;
Vue.prototype.handleTree = handleTree;

Vue.prototype.msgSuccess = function(msg) {
  this.$message({ showClose: true, message: msg, type: "success" });
};

Vue.prototype.msgError = function(msg) {
  this.$message({ showClose: true, message: msg, type: "error" });
};

Vue.prototype.msgInfo = function(msg) {
  this.$message.info(msg);
};

// 全局组件挂载
Vue.component("Pagination", Pagination);

Vue.use(permission);

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

Vue.use(Element, {
  size: Cookies.get("size") || "medium" // set element-ui default size
});
Vue.use(ElDialog);

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App)
});

Vue.filter("dateYMDHMSFormat", function(
  dateStr,
  pattern = "YYYY-MM-DD HH:mm:ss"
) {
  return moment(dateStr).format(pattern);
});

Vue.filter("outletFormat", function(
  dateStr
) {
  if (dateStr == undefined || dateStr == null || dateStr == '') {
    return ''
  }
  return dateStr
  // if (dateStr.length < 5) {
  //   return dateStr
  // }
  // return dateStr.substring(dateStr.length-5);
});

Vue.filter("dateYMDFormat", function(dateStr, pattern = "YYYY-MM-DD") {
  return moment(dateStr).format(pattern);
});

Vue.filter("dateYMDHMSFormat", function(dateStr, pattern = "YYYY-MM-DD HH:mm:ss") {
  return moment(dateStr).format(pattern);
});

Vue.directive("enterNumber", {
  inserted: function(el) {
    el.addEventListener("keypress", function(e) {
      e = e || window.event;
      let charcode = typeof e.charCode === "number" ? e.charCode : e.keyCode;
      let re = /\d/;
      if (
        !re.test(String.fromCharCode(charcode)) &&
        charcode > 9 &&
        !e.ctrlKey
      ) {
        if (e.preventDefault) {
          e.preventDefault();
        } else {
          e.returnValue = false;
        }
      }
    });
  }
});

//输入数字限制最大值
Vue.directive('enterNumberMax', {
  inserted: function (el, binding) {
    // binding.value
    let trigger = (el, type) => {
      const e = document.createEvent('HTMLEvents')
      e.initEvent(type, true, true)
      el.dispatchEvent(e)
    }

    el.addEventListener("keyup", function (e) {
      let input = e.target;
      let value = input.value;
      if (parseFloat(value) > parseFloat(binding.value)) {
        input.value = binding.value;
      } else {
        input.value = parseFloat(value)
      }
      trigger(input, 'input')
    });
  }
});
