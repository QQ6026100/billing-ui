import request from "@/utils/request";

// 查询专干任务进度
export function taskProcess(data) {
  return request({
    url: "/billing/monthUserTarget/taskProcess",
    method: "get",
    params: data
  });
}

// 导出任务进度
export function exportTaskProcess(data) {
  return request({
    url: "/billing/monthUserTarget/exportTaskProcess",
    method: "get",
    params: data
  });
}

// 专干列表
export function getFullTimeUsers(data) {
  return request({
    url: "/billing/monthUserTarget/getFullTimeUsers",
    method: "get",
    params: data
  });
}
