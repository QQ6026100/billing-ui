/**
 * 年度数据汇总
 */
// @GetMapping("/billing/homePage/dataSummary")

/**
 * 月度曲线数据汇总
 */
// @GetMapping("/billing/homePage/dataSummaryByMonth")

/**
 * 按地区数据汇总
 */
// @GetMapping("/billing/homePage/dataSummaryByArea")

/**
 * 统计各个区域下网点的销量和订单量
 */
// @GetMapping("/billing/homePage/dataSummaryByOutlet")
import request from "@/utils/request";

// 年度数据汇总
export function dataSummary(data) {
  return request({
    url: "/billing/homePage/dataSummary",
    method: "get",
    data: data
  });
}
// 月度曲线数据汇总
export function dataSummaryByMonth(data) {
  return request({
    url: "/billing/homePage/dataSummaryByMonth",
    method: "get",
    params: data
  });
}
// 按地区数据汇总
export function dataSummaryByArea(data) {
  return request({
    url: "/billing/homePage/dataSummaryByArea",
    method: "get",
    params: data
  });
}
// 统计各个区域下网点的销量和订单量
export function dataSummaryByOutlet(params) {
  return request({
    url: "/billing/homePage/dataSummaryByAreaAndMonth",
    method: "get",
    params
  });
}
