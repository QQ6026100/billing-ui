import request from '@/utils/request'

// 查询订单数据列表
export function listData(query) {
  return request({
    url: '/billing/orderData/list',
    method: 'get',
    params: query
  })
}

// 导入订单数据
export function importDayData(data) {
  return request({
    url: '/billing/orderData/importDayData',
    method: 'post',
    data: data
  })
}

// 订单数据导入确认
export function confirmDayData(data) {
  return request({
    url: '/billing/orderData/confirmDayData/',
    data: data,
    method: 'post'
  })
}