import request from '@/utils/request'

// 查询星级标准维护列表
export function listStandard(query) {
  return request({
    url: '/billing/standard/list',
    method: 'get',
    params: query
  })
}

// 查询星级标准维护详细
export function getStandard(id) {
  return request({
    url: '/billing/standard/' + id,
    method: 'get'
  })
}

// 新增星级标准维护
export function addStandard(data) {
  return request({
    url: '/billing/standard',
    method: 'post',
    data: data
  })
}

// 修改星级标准维护
export function updateStandard(data) {
  return request({
    url: '/billing/standard',
    method: 'put',
    data: data
  })
}

// 删除星级标准维护
export function delStandard(id) {
  return request({
    url: '/billing/standard/' + id,
    method: 'delete'
  })
}

// 导出星级标准维护
export function exportStandard(query) {
  return request({
    url: '/billing/standard/export',
    method: 'get',
    params: query
  })
}