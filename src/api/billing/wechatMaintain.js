import request from '@/utils/request'

// 查询微信维护列表
export function listMaintain(query) {
  return request({
    url: '/billing/wechatMaintain/list?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}


// 导出专干网点日常维护
export function exportMaintain(query) {
  return request({
    url: '/billing/wechatMaintain/export?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}

// 查询微信维护详细
export function getMaintain(id) {
  return request({
    url: '/billing/wechatMaintain/getInfo?id=' + id,
    method: 'get'
  })
}

// 新增微信维护
export function addMaintain(data) {
  return request({
    url: '/billing/maintain',
    method: 'post',
    data: data
  })
}

// 修改微信维护
export function updateMaintain(data) {
  return request({
    url: '/billing/maintain',
    method: 'put',
    data: data
  })
}

// 删除微信维护
export function delMaintain(id) {
  return request({
    url: '/billing/maintain/' + id,
    method: 'delete'
  })
}






// 任务类型
export function selectDictDataByType(dictType) {
  return request({
    url: '/system/dict/data/dictType/' + dictType,
    method: 'get'
  })
}