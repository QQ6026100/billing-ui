import request from '@/utils/request'

// 查询活动管理列表
export function listActivitys(query) {
  return request({
    url: '/billing/activitys/list',
    method: 'get',
    params: query
  })
}

// 查询活动管理详细
export function getActivitys(id) {
  return request({
    url: '/billing/activitys/' + id,
    method: 'get'
  })
}

// 新增活动管理
export function addActivitys(data) {
  return request({
    url: '/billing/activitys',
    method: 'post',
    data: data
  })
}

// 修改活动管理
export function updateActivitys(data) {
  return request({
    url: '/billing/activitys',
    method: 'put',
    data: data
  })
}

// 删除活动管理
export function delActivitys(id) {
  return request({
    url: '/billing/activitys/' + id,
    method: 'delete'
  })
}

// 导出活动管理
export function exportActivitys(query) {
  return request({
    url: '/billing/activitys/export',
    method: 'get',
    params: query
  })
}