import request from '@/utils/request'

// 查询消息人员关系列表
export function listUser(query) {
  return request({
    url: '/billing/user/list',
    method: 'get',
    params: query
  })
}

// 查询消息人员关系详细
export function getUser(id) {
  return request({
    url: '/billing/user/' + id,
    method: 'get'
  })
}

// 新增消息人员关系
export function addUser(data) {
  return request({
    url: '/billing/user',
    method: 'post',
    data: data
  })
}

// 修改消息人员关系
export function updateUser(data) {
  return request({
    url: '/billing/user',
    method: 'put',
    data: data
  })
}

// 删除消息人员关系
export function delUser(id) {
  return request({
    url: '/billing/user/' + id,
    method: 'delete'
  })
}

// 导出消息人员关系
export function exportUser(query) {
  return request({
    url: '/billing/user/export',
    method: 'get',
    params: query
  })
}