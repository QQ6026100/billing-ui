import request from '@/utils/request'

// 查询考勤打卡管理列表
export function listEveryclock(query) {
  return request({
    url: '/billing/everyclock/list',
    method: 'get',
    params: query
  })
}

// 查询考勤打卡管理详细
export function getEveryclock(id) {
  return request({
    url: '/billing/everyclock/' + id,
    method: 'get'
  })
}

// 新增考勤打卡管理
export function addEveryclock(data) {
  return request({
    url: '/billing/everyclock',
    method: 'post',
    data: data
  })
}

// 修改考勤打卡管理
export function updateEveryclock(data) {
  return request({
    url: '/billing/everyclock',
    method: 'put',
    data: data
  })
}

// 删除考勤打卡管理
export function delEveryclock(id) {
  return request({
    url: '/billing/everyclock/' + id,
    method: 'delete'
  })
}

// 导出考勤打卡管理
export function exportEveryclock(query) {
  return request({
    url: '/billing/everyclock/export',
    method: 'get',
    params: query
  })
}