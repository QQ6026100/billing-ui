import request from '@/utils/request'

// 新增月度专干指标数据
export function addData(data) {
  return request({
    url: '/billing/monthTarget/add',
    method: 'post',
    data: data
  })
}

// 新增月度专干指标数据
export function addUserData(data) {
  return request({
    url: '/billing/monthUserTarget/add',
    method: 'post',
    data: data
  })
}

// 新增月度专干指标数据
export function updateData(data) {
  return request({
    url: '/billing/monthTarget/update',
    method: 'post',
    data: data
  })
}




// 查询月度专干指标数据列表
export function listData(query) {
  return request({
    url: '/billing/monthUserTarget/monthTargetData',
    method: 'get',
    params: query
  })
}

// 查询月度专干数据分析
export function getUserDataAnalysis(query) {
  return request({
    url: '/billing/monthUserTarget/userDataAnalysis',
    method: 'get',
    params: query
  })
}
// 导出月度专干数据分析
export function exportUserDataAnalysis(query) {
  return request({
    url: '/billing/monthUserTarget/exportUserDataAnalysis',
    method: 'get',
    params: query
  })
}


// // 查询月度专干指标数据详细
// export function getData(id) {
//   return request({
//     url: '/billing/data/' + id,
//     method: 'get'
//   })
// }

// // 修改月度专干指标数据
// export function updateData(data) {
//   return request({
//     url: '/billing/data',
//     method: 'put',
//     data: data
//   })
// }

// // 删除月度专干指标数据
// export function delData(id) {
//   return request({
//     url: '/billing/data/' + id,
//     method: 'delete'
//   })
// }

// // 导出月度专干指标数据
// export function exportData(query) {
//   return request({
//     url: '/billing/data/export',
//     method: 'get',
//     params: query
//   })
// }