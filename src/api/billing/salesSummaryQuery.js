import request from '@/utils/request'

// 销量数据汇总查询
export function salesSummaryDateQuery(query) {
  return request({
    url: '/billing/salesData/summaryList',
    method: 'get',
    params: query
  })
}