import request from '@/utils/request'

// 日考核
export function dayDataList(query) {
  return request({
    url: '/billing/userAssessment/dayDataList',
    method: 'get',
    params: query
  })
}

// 月周数
export function getWeeksByMonth(query) {
  return request({
    url: '/billing/userAssessment/getWeeksByMonth',
    method: 'get',
    params: query
  })
}

// 周考核
export function weekDataList(query) {
  return request({
    url: '/billing/userAssessment/weekDataList',
    method: 'get',
    params: query
  })
}

// 月考核
export function monthDataList(query) {
  return request({
    url: '/billing/userAssessment/monthDataList',
    method: 'get',
    params: query
  })
}

// 月考核统计结果
export function monthScoreList(query) {
  return request({
    url: '/billing/assessmentResults/monthScoreList',
    method: 'get',
    params: query
  })
}

// 月考核结果得分
export function monthResultScoreList(query) {
  return request({
    url: '/billing/assessmentResults/monthResultScoreList',
    method: 'get',
    params: query
  })
}

// 导出月度考核结果
export function exportResults(query) {
  return request({
    url: '/billing/assessmentResults/export',
    method: 'get',
    params: query
  })
}

// 月考核结果保存
export function addMonthScore(data) {
  return request({
    url: '/billing/assessmentResults/add',
    method: 'post',
    data: data
  })
}


