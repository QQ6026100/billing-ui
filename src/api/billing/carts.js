import request from '@/utils/request'

// 查询购物车列表
export function listCarts(query) {
  return request({
    url: '/billing/carts/list',
    method: 'get',
    params: query
  })
}

// 查询购物车详细
export function getCarts(id) {
  return request({
    url: '/billing/carts/' + id,
    method: 'get'
  })
}

// 新增购物车
export function addCarts(data) {
  return request({
    url: '/billing/carts',
    method: 'post',
    data: data
  })
}

// 修改购物车
export function updateCarts(data) {
  return request({
    url: '/billing/carts',
    method: 'put',
    data: data
  })
}

// 删除购物车
export function delCarts(id) {
  return request({
    url: '/billing/carts/' + id,
    method: 'delete'
  })
}

// 导出购物车
export function exportCarts(query) {
  return request({
    url: '/billing/carts/export',
    method: 'get',
    params: query
  })
}