import request from '@/utils/request'

// 查询专干网点日常维护列表
export function listMaintain(query) {
  return request({
    url: '/billing/maintain/list?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}

// 查询专干网点日常维护详细
export function getMaintain(id) {
  return request({
    url: '/billing/maintain/getInfo?id=' + id,
    method: 'get'
  })
}

// 新增专干网点日常维护
export function addMaintain(data) {
  return request({
    url: '/billing/maintain',
    method: 'post',
    data: data
  })
}

// 修改专干网点日常维护
export function updateMaintain(data) {
  return request({
    url: '/billing/maintain',
    method: 'put',
    data: data
  })
}

// 删除专干网点日常维护
export function delMaintain(id) {
  return request({
    url: '/billing/maintain/' + id,
    method: 'delete'
  })
}

// 导出专干网点日常维护
export function exportMaintain(query) {
  return request({
    url: '/billing/maintain/export?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}