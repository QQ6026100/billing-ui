import request from '@/utils/request'

// 查询销量数据年关系列表
export function listYearRelation(query) {
  return request({
    url: '/billing/yearRelation/list',
    method: 'get',
    params: query
  })
}

// Excel数据导入
export function importYearData(data) {
  return request({
    url: '/billing/yearRelation/importYearData/',
    data: data,
    method: 'post'
  })
}

// 确认
export function confirmYearData(data) {
  return request({
    url: '/billing/yearRelation/confirmYearData/',
    data: data,
    method: 'post'
  })
}