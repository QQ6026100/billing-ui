import request from '@/utils/request'

// 查询任务管理列表
export function listManagement(query) {
  return request({
    url: '/billing/management/list?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}

// 查询网点
export function getTcOutlets() {
  return request({
    url: '/billing/management/getTcOutlets',
    method: 'get'
  })
}

// 任务类型
export function selectDictDataByType(dictType) {
  return request({
    url: '/system/dict/data/dictType/' + dictType,
    method: 'get'
  })
}

// 查询任务管理详细
export function getManagement(id) {
  return request({
    url: '/billing/management/getInfo?id=' + id,
    method: 'get'
  })
}

// 新增任务管理
export function addManagement(data) {
  return request({
    url: '/billing/management',
    method: 'post',
    data: data
  })
}

// 修改任务管理
export function updateManagement(data) {
  return request({
    url: '/billing/management',
    method: 'put',
    data: data
  })
}

// 删除任务管理
export function delManagement(id) {
  return request({
    url: '/billing/management/' + id,
    method: 'delete'
  })
}

// 导出任务管理
export function exportManagement(query) {
  return request({
    url: '/billing/management/export',
    method: 'get',
    params: query
  })
}