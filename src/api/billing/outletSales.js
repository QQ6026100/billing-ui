import request from '@/utils/request'

// 网点销量预警接口数据
export function outletSalesQuery(query) {
  return request({
    url: '/billing/salesData/outletSalesQuery',
    method: 'get',
    params: query
  })
}

// 导出网点库存
export function exportSaleWarningData(query) {
  return request({
    url: '/billing/salesData/exportSaleWarningData',
    method: 'get',
    params: query
  })
}