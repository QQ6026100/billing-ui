import request from '@/utils/request'

// 查询网点管理列表
export function listOutlets(query) {
  return request({
    url: '/billing/outlets/list?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}


// 查询网点管理列表
export function getOutletsWarningList(query) {
  return request({
    url: '/billing/outlets/outletsWarningList',
    method: 'get',
    params: query
  })
}

// 导出网点管理列表
export function exportOutletsWarningList(query) {
  return request({
    url: '/billing/outlets/exportOutletsWarningList',
    method: 'get',
    params: query
  })
}


// 查询专干人员
export function selectSpecializedPersonnel() {
  return request({
    url: '/billing/outlets/selectSpecializedPersonnel',
    method: 'get'
  })
}

// 查询专干人员
export function selectSpecializedPersonnelList() {
  return request({
    url: '/billing/outlets/selectSpecializedPersonnelList',
    method: 'get'
  })
}

// 查询网点管理详细
export function getOutlets(id) {
  return request({
    url: '/billing/outlets/getInfo?id=' + id,
    method: 'get'
  })
}

// 新增网点管理
export function addOutlets(data) {
  return request({
    url: '/billing/outlets/add',
    method: 'post',
    data: data
  })
}


// 新增网点管理
export function save(data) {
  return request({
    url: '/billing/outlets/save',
    method: 'post',
    data: data
  })
}

// 修改网点管理
export function updateOutlets(data) {
  return request({
    url: '/billing/outlets/edit',
    method: 'post',
    data: data
  })
}

// 删除网点管理
export function delOutlets(data) {
  return request({
    url: '/billing/outlets/remove',
    method: 'post',
    data: data
  })
}


// 解除
export function relieve(data) {
  return request({
    url: '/billing/outlets/relieve',
    method: 'post',
    data: data
  })
}


// 解除
export function relieveByUserId(data) {
  return request({
    url: '/billing/outlets/relieveByUserId',
    method: 'post',
    data: data
  })
}



// 绑定
export function batchBinding(data, userId) {
  return request({
    url: '/billing/outlets/batchBinding?userId=' + userId,
    method: 'post',
    data: data
  })
}

// 导出网点管理
export function exportOutlets(query) {
  return request({
    url: '/billing/outlets/export?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}



// 查询所有省
export function selectProvinces(regionLevel) {
  return request({
    url: '/billing/outlets/selectProvinces?regionLevel=' + regionLevel,
    method: 'get'
  })
}


// 查询查询所有市或者县
export function selectCityOrCounty(parentRegionCode) {
  return request({
    url: '/billing/outlets/selectCityOrCounty?parentRegionCode=' + parentRegionCode,
    method: 'get'
  })
}


// 查询网点类别
export function selectOutletTpye() {
  return request({
    url: '/billing/outlets/selectOutletTpye',
    method: 'get'
  })
}


// 查询人员
export function selectUser() {
  return request({
    url: '/billing/outlets/selectUser',
    method: 'get'
  })
}


// 查询星级
export function selectStarRating() {
  return request({
    url: '/billing/outlets/selectStarRating',
    method: 'get'
  })
}


// 查询星级
export function selectUnallocated(query) {
  return request({
    url: '/billing/outlets/selectUnallocated?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}


// 未建网点分析
export function selectNotBuiltOutlets(query) {
  return request({
    url: '/billing/outlets/selectNotBuiltOutlets?pageNum=' + query.pageNum + '&pageSize=' + query.pageSize,
    method: 'post',
    data: query
  })
}




// 重置密码
export function updatePartyPassword(data) {
  return request({
    url: '/billing/outlets/updatePartyPassword',
    method: 'post',
    data: data
  })
}


// 查询网点类别
export function getList(data) {
  return request({
    url: '/billing/local/getList',
    method: 'post',
    data: data
  })
}

