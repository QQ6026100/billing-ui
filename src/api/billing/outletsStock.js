import request from '@/utils/request'

// 查询网点库存列表
export function listStock(query) {
  return request({
    url: '/billing/stock/list',
    method: 'get',
    params: query
  })
}

// 查询网点库存履历列表
export function getStockHistoryList(query) {
  return request({
    url: '/billing/history/list',
    method: 'get',
    params: query
  })
}

// 查询预约记录
export function getBookingRecord(query) {
  return request({
    url: '/billing/track/getBookingRecord',
    method: 'get',
    params: query
  })
}

// 新增任务管理
export function taskAssignAdd(data) {
  return request({
    url: '/billing/management',
    method: 'post',
    data: data
  })
}
// 是否存在未完成的任务
export function taskVerification(query) {
  return request({
    url: '/billing/management/taskVerification?userId='+query.userId+'&outletId='+query.outletId+'&taskType='+query.taskType+'&stateFlg='+query.stateFlg,
    method: 'get'
  })
}

// 检索是否存在未完成的订单任务
export function notCompleteTaskCnt(data) {
  return request({
    url: '/billing/management/notCompleteTaskCnt',
    method: 'post',
    data: data
  })
}

// 导出网点库存
export function exportStock(query) {
  return request({
    url: '/billing/stock/export',
    method: 'get',
    params: query
  })
}