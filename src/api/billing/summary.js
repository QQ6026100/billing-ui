import request from '@/utils/request'

// 查询计划总结列表
export function listSummary(query) {
  return request({
    url: '/billing/summary/list',
    method: 'get',
    params: query
  })
}

// 查询计划总结详细
export function getSummary(id) {
  return request({
    url: '/billing/summary/' + id,
    method: 'get'
  })
}

// 新增计划总结
export function addSummary(data) {
  return request({
    url: '/billing/summary',
    method: 'post',
    data: data
  })
}

// 修改计划总结
export function updateSummary(data) {
  return request({
    url: '/billing/summary',
    method: 'put',
    data: data
  })
}

// 删除计划总结
export function delSummary(id) {
  return request({
    url: '/billing/summary/' + id,
    method: 'delete'
  })
}

// 导出计划总结
export function exportSummary(query) {
  return request({
    url: '/billing/summary/export',
    method: 'get',
    params: query
  })
}