import request from '@/utils/request'

// 查询订票跟踪列表
export function listTrack(query) {
  return request({
    url: '/billing/track/list',
    method: 'get',
    params: query
  })
}

// 查询订票跟踪详细
export function getTrack(id) {
  return request({
    url: '/billing/track/' + id,
    method: 'get'
  })
}

// 新增订票跟踪
export function addTrack(data) {
  return request({
    url: '/billing/track',
    method: 'post',
    data: data
  })
}

// 修改订票跟踪
export function updateTrack(data) {
  return request({
    url: '/billing/track',
    method: 'put',
    data: data
  })
}

// 删除订票跟踪
export function delTrack(id) {
  return request({
    url: '/billing/track/' + id,
    method: 'delete'
  })
}

// 导出订票跟踪
export function exportTrack(query) {
  return request({
    url: '/billing/track/export',
    method: 'get',
    params: query
  })
}

// 订票预警新增消息
export function outletTrackAddNews(data) {
  return request({
    url: '/billing/track/outletTrackAddNews',
    method: 'post',
    data: data
  })
}