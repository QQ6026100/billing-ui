import request from '@/utils/request'

// 查询消息管理列表
export function listNews(query) {
  return request({
    url: '/billing/news/list',
    method: 'get',
    params: query
  })
}

// 查询消息管理详细
export function getNews(id) {
  return request({
    url: '/billing/news/' + id,
    method: 'get'
  })
}

// 新增消息管理
export function addNews(data) {
  return request({
    url: '/billing/news',
    method: 'post',
    data: data
  })
}

// 修改消息管理
export function updateNews(data) {
  return request({
    url: '/billing/news',
    method: 'put',
    data: data
  })
}

// 删除消息管理
export function delNews(id) {
  return request({
    url: '/billing/news/' + id,
    method: 'delete'
  })
}

// 导出消息管理
export function exportNews(query) {
  return request({
    url: '/billing/news/export',
    method: 'get',
    params: query
  })
}