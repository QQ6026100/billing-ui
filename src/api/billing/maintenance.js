import request from "@/utils/request";

export function calendarMake(query) {
  return request({
    url: "/billing/calendar/make",
    method: "get",
    params: query
  });
}
