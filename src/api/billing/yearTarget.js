import request from '@/utils/request'

// 新增年度专干指标数据
export function addData(data) {
  return request({
    url: '/billing/yearTarget/add',
    method: 'post',
    data: data
  })
}

// 保存指定
export function saveAndMakeData(data) {
  return request({
    url: '/billing/yearUserTarget/add',
    method: 'post',
    data: data
  })
}

// 修改年度专干指标数据
export function updateData(data) {
  return request({
    url: '/billing/yearTarget/update',
    method: 'post',
    data: data
  })
}

// 查询年度专干指标数据列表
export function listData(query) {
  return request({
    url: '/billing/yearTarget/yearTargetData',
    method: 'get',
    params: query
  })
}

// // 查询年度专干指标数据详细
// export function getData(id) {
//   return request({
//     url: '/billing/data/' + id,
//     method: 'get'
//   })
// }

// // 修改年度专干指标数据
// export function updateData(data) {
//   return request({
//     url: '/billing/data',
//     method: 'put',
//     data: data
//   })
// }

// // 删除年度专干指标数据
// export function delData(id) {
//   return request({
//     url: '/billing/data/' + id,
//     method: 'delete'
//   })
// }

// // 导出年度专干指标数据
// export function exportData(query) {
//   return request({
//     url: '/billing/data/export',
//     method: 'get',
//     params: query
//   })
// }