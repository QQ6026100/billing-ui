import request from '@/utils/request'

// 查询销量数据年列表
export function salesYearDateQuery(query) {
  return request({
    url: '/billing/yearData/list',
    method: 'get',
    params: query
  })
}

// 查询销量数据月列表
export function salesMonthDateQuery(query) {
  return request({
    url: '/billing/monthData/list',
    method: 'get',
    params: query
  })
}

// 查询销量数据日列表
export function salesDayDateQuery(data) {
  return request({
    url: '/billing/salesData/list',
    method: 'get',
    params: data
  })
}

// Excel数据导入
export function importDayData(data) {
  return request({
    url: '/billing/salesData/importDayData/',
    data: data,
    method: 'post'
  })
}

// 销量数据导入确认
export function confirmDayData(data) {
  return request({
    url: '/billing/salesData/confirmDayData/',
    data: data,
    method: 'post'
  })
}

// 销量数据月度汇总统计
export function dataSummaryByMonth(data) {
  return request({
    url: '/billing/salesData/dataSummaryByMonth',
    method: 'post',
    data: data
  })
}

// 销量数据年度汇总统计
export function dataSummaryByYear(data) {
  return request({
    url: '/billing/salesData/dataSummaryByYear',
    method: 'post',
    data: data
  })
}