import request from '@/utils/request'

// 查询体彩日历列表
export function listCalendar(data) {
  return request({
    url: '/billing/calendar/list',
    method: 'post',
    data
  })
}

// 查询体彩日历详细
export function getCalendar(id) {
  return request({
    url: '/billing/calendar/' + id,
    method: 'get'
  })
}

// 新增体彩日历
export function addCalendar(data) {
  return request({
    url: '/billing/calendar',
    method: 'post',
    data: data
  })
}

// 修改体彩日历
export function updateCalendar(data) {
  return request({
    url: '/billing/calendar',
    method: 'put',
    data: data
  })
}

// 删除体彩日历
export function delCalendar(id) {
  return request({
    url: '/billing/calendar/' + id,
    method: 'delete'
  })
}

// 导出体彩日历
export function exportCalendar(query) {
  return request({
    url: '/billing/calendar/export',
    method: 'get',
    params: query
  })
}

// 生成体彩日历
export function makeCalendar(data) {
  return request({
    url: '/billing/calendar/make',
    method: 'get',
    params: data
  })
}