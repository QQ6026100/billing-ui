import request from '@/utils/request'

// 查询打卡区域管理列表
export function listClock(query) {
  return request({
    url: '/billing/clock/list',
    method: 'get',
    params: query
  })
}

// 查询打卡区域管理详细
export function getClock(id) {
  return request({
    url: '/billing/clock/' + id,
    method: 'get'
  })
}

// 新增打卡区域管理
export function addClock(data) {
  return request({
    url: '/billing/clock',
    method: 'post',
    data: data
  })
}

// 修改打卡区域管理
export function updateClock(data) {
  return request({
    url: '/billing/clock',
    method: 'put',
    data: data
  })
}

// 删除打卡区域管理
export function delClock(id) {
  return request({
    url: '/billing/clock/' + id,
    method: 'delete'
  })
}

// 导出打卡区域管理
export function exportClock(query) {
  return request({
    url: '/billing/clock/export',
    method: 'get',
    params: query
  })
}