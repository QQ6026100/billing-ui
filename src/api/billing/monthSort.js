import request from '@/utils/request'

// 查询网点月度分类列表
export function listMonthSort(query) {
  return request({
    url: '/billing/monthSort/list',
    method: 'get',
    params: query
  })
}

// 查询网点月度销量分析，同比环比
export function getOutletsMonthDataAnalysisList(query) {
  return request({
    url: '/billing/monthSort/getOutletsMonthDataAnalysisList',
    method: 'get',
    params: query
  })
}

// 查询网点月度销量分析，同比环比  数据导出
export function exportOutletsDataAnalysis(query) {
  return request({
    url: '/billing/monthSort/exportOutletsDataAnalysis',
    method: 'get',
    params: query
  })
}

// 查询网点月度分类列表：按区县统计
export function getOutletsMonthDataGroupByCounty(query) {
  return request({
    url: '/billing/monthSort/getOutletsMonthDataGroupByCounty',
    method: 'get',
    params: query
  })
}

// 查询网点月度分类列表：按区县统计
export function exportOutletsMonthDataGroupByCounty(query) {
  return request({
    url: '/billing/monthSort/exportOutletsMonthDataGroupByCounty',
    method: 'get',
    params: query
  })
}

// 查询网点月度分类列表：按区域统计
export function getOutletsDataGroupByCity(query) {
  return request({
    url: '/billing/monthSort/getOutletsDataGroupByCity',
    method: 'get',
    params: query
  })
}

// 查询网点月度分类列表：按区域统计
export function exportOutletsDataGroupByCity(query) {
  return request({
    url: '/billing/monthSort/exportOutletsDataGroupByCity',
    method: 'get',
    params: query
  })
}

