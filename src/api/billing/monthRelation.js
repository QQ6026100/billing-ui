import request from '@/utils/request'

// 查询销量数据月关系列表
export function listMonthRelation(query) {
  return request({
    url: '/billing/monthRelation/list',
    method: 'get',
    params: query
  })
}

// Excel数据导入
export function importMonthData(data) {
  return request({
    url: '/billing/monthRelation/importMonthData/',
    data: data,
    method: 'post'
  })
}

// 确认
export function confirmMonthData(data) {
  return request({
    url: '/billing/monthRelation/confirmMonthData/',
    data: data,
    method: 'post'
  })
}