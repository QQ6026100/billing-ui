import request from '@/utils/request'

// 查询申请报备列表
export function listGuidance(query) {
  return request({
    url: '/billing/guidance/list',
    method: 'get',
    params: query
  })
}

// 查询申请报备详细
export function getGuidance(id) {
  return request({
    url: '/billing/guidance/' + id,
    method: 'get'
  })
}

// 新增申请报备
export function addGuidance(data) {
  return request({
    url: '/billing/guidance',
    method: 'post',
    data: data
  })
}

// 修改申请报备
export function updateGuidance(data) {
  return request({
    url: '/billing/guidance',
    method: 'put',
    data: data
  })
}

// 删除申请报备
export function delGuidance(id) {
  return request({
    url: '/billing/guidance/' + id,
    method: 'delete'
  })
}

// 导出申请报备
export function exportGuidance(query) {
  return request({
    url: '/billing/guidance/export',
    method: 'get',
    params: query
  })
}