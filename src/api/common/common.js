import request from '@/utils/request'

// 图片删除接口
export function removeFile(data) {
  return request({
    url: '/common/billing/remove',
    method: 'post',
    data: data
  })
}

// 行政区域
export function getDivisionProvinces(regionLevel) {
  return request({
    url: '/billing/outlets/selectProvinces?regionLevel=' + regionLevel,
    method: 'get'
  })
}

// 行政区域
export function getDivisionCitys(parentRegionCode) {
  return request({
    url: '/billing/outlets/selectCityOrCounty?parentRegionCode=' + parentRegionCode,
    method: 'get'
  })
}